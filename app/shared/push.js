var firebase           = require("nativescript-plugin-firebase");
var dialogs            = require("tns-core-modules/ui/dialogs");
var { device }         = require("tns-core-modules/platform");
var { webappTokenUrl } = require('./urls');

exports.register = function() {
    return firebase.init({
        onPushTokenReceivedCallback: token => {
            console.log('New device token: ', token);
            /*
             * This update might be redundat, since we still set the token
             * inside the webview. Howerver it may be called in a scenario
             * where the webview will not be opened (background update?).
             * Setting the token several times is ok, though, and it will
             * only run twice when the app is first opened.
             */
            updatePushToken(token);
        }
    }).then(instance => {
        console.log('Firebase initialized');
    }).catch(err => {
        console.log('Failed to initialize Firebase: ' + err)
    })
}

exports.displayNotification = function(notification) {
    var options = {
        title: notification.title,
        message: notification.body,
        okButtonText: notification.okText,
        neutralButtonText: notification.closeText
    };
    return new Promise((resolve) => {
        dialogs.confirm(options).then(result => {
            resolve(result)
        });
    });
}

function updatePushToken(token) {
    console.log('Sending token to remote server...');
    fetch(webappTokenUrl, {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({ token: token, uuid: device.uuid })
    }).then(response => {
        if (response.ok) {
            console.log('Token was set on remote server.');
        } else {
          throw Error(response.statusText);
        }
    }).catch(error => {
        console.log('Failed to set token: ' + error);
    });
}

function retrieveToken(retries) {
    retries = retries || 1;

    console.log('Trying to retrieve token... Attempt #' + retries)
    return new Promise((resolve) => {
        firebase.getCurrentPushToken().then((token) => {
            if (token) {
                resolve(token);
            } else if (retries < 3) {
                setTimeout(() => {
                    resolve(retrieveToken(retries + 1));
                }, 15000);
            } else {
                resolve(null)
            }
        });
    });
}

exports.retrieveToken = retrieveToken;
