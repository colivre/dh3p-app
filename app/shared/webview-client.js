var app = require("tns-core-modules/application");

var Intent = android.content.Intent;

var GET_CONTENT_REQUEST_CODE = 500;

function WebViewClient(progressBar) {
    var client = android.webkit.WebChromeClient.extend({
        onShowFileChooser: function(webView, resultCallback, fileChooserParams) {
            app.android.foregroundActivity.fileInputCallback = resultCallback;

            var getContentIntent = new Intent(Intent.ACTION_GET_CONTENT);
            getContentIntent.addCategory(Intent.CATEGORY_OPENABLE);
            getContentIntent.setType("*/*");

            var chooserIntent = new Intent(Intent.ACTION_CHOOSER);
            chooserIntent.putExtra(Intent.EXTRA_INTENT, getContentIntent);

            var activity = app.android.foregroundActivity
            activity.startActivityForResult(chooserIntent,
                                            GET_CONTENT_REQUEST_CODE);
            return true;
        },

        onProgressChanged: function(webView, progress) {
            if (progressBar) {
                progressBar.set("progress", progress);
            }
        },

        onGeolocationPermissionsShowPrompt: function(origin, callback) {
          callback.invoke(origin, true, false);
        }
    });
    return new client();
}

exports.WebViewClient = WebViewClient;

exports.GET_CONTENT_REQUEST_CODE = GET_CONTENT_REQUEST_CODE;
