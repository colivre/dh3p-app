
// TODO: add env configs
exports.config = {
    webapp: {
      protocol: 'https',
      host: 'participamais.mg.gov.br',
      port: '',
      endpoint: '/',
      tokenEndpoint: '/plugin/denouncements/push_token/set',
      denouncementsEndpoint: '/plugin/denouncements'
    },

    firebase: {
        projectNumber: 'YOUR_PROJECT_NUM'
    },

    notifications: {
      newContent: 'NEW_CONTENT',
      denouncementUpdate: 'DENOUNCEMENT_UPDATE'
    }
}
