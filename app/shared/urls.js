var webapp = require('./config').config.webapp;

var webappUrl = `${webapp.protocol}://${webapp.host}:${webapp.port}`;
exports.webappUrl = webappUrl + webapp.endpoint;
exports.webappTokenUrl = webappUrl + webapp.tokenEndpoint;
exports.denouncementsUrl = webappUrl + webapp.denouncementsEndpoint;

exports.trackingUrl = (protocol) => {
    var params = ''
    if (protocol) {
        params = '?protocol=' + protocol;
    }
    return webappUrl + webapp.denouncementsEndpoint + params;
}

exports.contentUrl = (path) => {
  path = path || '';
  return webappUrl + path;
}
