var frame                        = require("ui/frame");
var { GET_CONTENT_REQUEST_CODE } = require('./shared/webview-client');

var superProto = android.app.Activity.prototype;
var Activity = android.app.Activity.extend("dh3p.MainActivity", {
    onCreate: function(savedInstanceState) {
        if(!this._callbacks) {
            frame.setActivityCallbacks(this);
        }
        this._callbacks.onCreate(this, savedInstanceState, superProto.onCreate);
    },

    onSaveInstanceState: function(outState) {
        this._callbacks.onSaveInstanceState(this, outState,
                                            superProto.onSaveInstanceState);
    },

    onStart: function() {
        this._callbacks.onStart(this, superProto.onStart);
    },

    onStop: function() {
        this._callbacks.onStop(this, superProto.onStop);
    },

    onDestroy: function() {
        this._callbacks.onDestroy(this, superProto.onDestroy);
    },

    onBackPressed: function() {
        if (!this.onBackCallback || !this.onBackCallback()) {
            this._callbacks.onBackPressed(this, superProto.onBackPressed);
        }
    },

    onRequestPermissionsResult: function (requestCode, permissions, results) {
        this._callbacks.onRequestPermissionsResult(this, requestCode,
                                                   permissions, results,
                                                   undefined);
    },

    onActivityResult: function (requestCode, resultCode, intent) {
        this._callbacks.onActivityResult(this, requestCode, resultCode, intent,
                                         superProto.onActivityResult);

        if (requestCode == GET_CONTENT_REQUEST_CODE) {
            if (resultCode == android.app.Activity.RESULT_OK) {
                var files = Array.create(android.net.Uri, 1);
                files[0] = android.net.Uri.parse(intent.getDataString())
                this.fileInputCallback.onReceiveValue(files)
            } else {
                this.fileInputCallback.onReceiveValue(null)
            }
        }
    }
});
