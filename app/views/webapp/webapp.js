var app                            = require("tns-core-modules/application");
var observable                     = require("data/observable");
var firebase                       = require("nativescript-plugin-firebase");
var push                           = require('../../shared/push');
var urls                           = require('../../shared/urls');
var { screen, device, isAndroid } = require("tns-core-modules/platform");
var geolocation                    = require("nativescript-geolocation");
var { notifications }              = require('../../shared/config').config;

// WebView's loadFinished may fire several times, depending on the connection
var webViewFirstLoad = true;

if (isAndroid) {
  var { WebViewClient }  = require('../../shared/webview-client');
}

function pageLoaded(args) {
    var page = args.object;
    var webView = page.getViewById('webView');

    if (webView.android) {
        var progressBar = new observable.fromObject({
            progress: 0
        });
        page.bindingContext = progressBar;

        webView.android.getSettings().setBuiltInZoomControls(false);
        webView.android.setWebChromeClient(new WebViewClient(progressBar));
    }

    requestLocation();

    // We could start loading the page before registering w/ Firebase,
    // but that creates a race condition. The current logic doesn't work
    // if the website finishes loading and we do not have the token yet.
    push.register().then(() => {
        firebase.addOnMessageReceivedCallback(message => {
            handleNotification(message, webView);
        });
    }).then(() => {
        // The webView src might have been set by the background notification
        webView.src = webView.src || urls.webappUrl;
        webView.on("loadFinished", args => {
            if (webViewFirstLoad) {
                webViewFirstLoad = false;
                // Let's stop scalling for now
                // setAndroidWebviewScale(webView);
                push.retrieveToken().then(token => {
                    setAndroidPushToken(webView, token);
                    webView.off('loadFinished');
                });
            }
        });
    });

    if (isAndroid) {
        app.android.foregroundActivity.onBackCallback = () => {
          if (webView.canGoBack) {
              webView.goBack();
              return true;
          } else {
              return false;
          }
        }
    }
}

// The webapp may not scale correctly, so we scale it manually
function setAndroidWebviewScale(webView) {
    if (webView.android) {
        var webViewWidth = webView.getActualSize().width;
        var screenWidth = screen.mainScreen.widthPixels;
        if (webViewWidth > 540) {
            var scale = Math.ceil((screenWidth / webViewWidth) * 115); // x115%
            webView.android.setInitialScale(scale);
        }
    }
}

function requestLocation() {
    // We are only interested in the permission request
    if (!geolocation.isEnabled()) {
          geolocation.enableLocationRequest();
    }
    geolocation.getCurrentLocation()
}

function setAndroidPushToken(webView, token) {
    if (webView.android && token) {
        console.log('Sending Token: ' + token);
        var script = `setPushToken("${token}", "${device.uuid}");`;
        webView.android.evaluateJavascript(script, null);
    } else if (webView.android) {
        console.log('Unable to retrieve token. Sending only device UUID.');
        var script = `setPushToken(null, "${device.uuid}");`;
        webView.android.evaluateJavascript(script, null);
    }
}

function handleNotification(message, webView) {
    var type = message.data.type;
    if (message.foreground) {
        var url = webView.src;
        var notification = {
            title: message.title,
            body: message.body,
            closeText: 'Fechar'
        };

        if (type == notifications.newContent) {
            notification.okText = 'Ver conteúdo';
            url = urls.contentUrl(message.data.path);
        } else if (type == notifications.denouncementUpdate) {
            notification.okText = 'Ver denúncia';
            url = urls.trackingUrl(message.data.protocol);
        }
        push.displayNotification(notification).then(result => {
            if (result) {
                webView.src = url;
            }
        });
    } else {
        if (type == notifications.newContent) {
            webView.src = urls.contentUrl(message.data.path);
        } else if (type == notifications.denouncementUpdate) {
            webView.src = urls.trackingUrl(message.data.protocol);
        }
    }
}

exports.pageLoaded = pageLoaded;
